package com.example.practica3

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.practica3.ui.TimePickerFragment
import kotlinx.android.synthetic.main.activity_picker_time.*

class PickerTimeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_time)
    }

    fun showTimePickerDialog(v: View) {
        //val timerPickerFragment = TimePickerFragment()
        val timePickerFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener
        { view, hourOfDay, minute ->
            pkrTime.setText("${hourOfDay}:${minute}")


        }
        )

        timePickerFragment.show(supportFragmentManager, "datePicker")

            }
}